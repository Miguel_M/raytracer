#include "Vector3D.h"
#include "Esfera.h"
#include "ViewPlane.h"
#include "Utilitarios.h"
#include "LuzPuntual.h"
#include "Triangulo.h"

#include "Image.h"
#include <math.h>
#include <vector>
#include <iostream>
using namespace std;

// #include "ObjetoGeometrico.h"


ColorRGB obtenerColorPixel(const Rayo& r, vector<ObjetoGeometrico*> objetos, LuzPuntual luz){
    
    ColorRGB color;
    color.r = 0.0;
    color.g = 0.0;
    color.b = 0.0;
    double t;
    double tmin = 2000000;    
    Vector3D n;
    Punto3D q;
 

    ColorRGB finalColor;
    finalColor.r=1.0;
    finalColor.g=1.0;
    finalColor.b=1.0;
    double u;
    double v;
    int column;
    int row;
    int px;
    double PI = 3.14159265358979323846;
    Vector3D h;

    for(int i = 0; i < objetos.size(); i++) {
        if( objetos[i]->hayImpacto(r, t, n, q) && t < tmin){
            u = objetos[i]->calcularU(n);
            v = objetos[i]->calcularV(n);
            if(objetos[i]->tieneTextura()){
                finalColor=objetos[i]->obtenerColorTextura(u,v); 
            }
            else{
                finalColor=objetos[i]->obtenerColor();
            }
            LuzPuntual luzAmbiente = LuzPuntual(0.4, 0.3, 0.8, 800.0, 0.0, -10.0);
            color.r = finalColor.r * luzAmbiente.color.r;
            color.g = finalColor.g * luzAmbiente.color.g;
            color.b = finalColor.b * luzAmbiente.color.b;
            h = ((-1)*r.d).hat();
            color.r = finalColor.r * luzAmbiente.color.r + finalColor.r * luz.color.r * std::max(0.0, n * (luz.posicion - q).hat() ) + finalColor.r * luz.color.r * pow(std::max(0.0, n * ((-1)*r.d + (luz.posicion - q).hat()).hat() ),100);
            color.g = finalColor.g * luzAmbiente.color.g + finalColor.g * luz.color.g * std::max(0.0, n * (luz.posicion - q).hat() ) + finalColor.g * luz.color.g * pow(std::max(0.0, n * ((-1)*r.d + (luz.posicion - q).hat()).hat() ),100);
            color.b = finalColor.b * luzAmbiente.color.b + finalColor.b * luz.color.b * std::max(0.0, n * (luz.posicion - q).hat() ) + finalColor.b * luz.color.b * pow(std::max(0.0, n * ((-1)*r.d + (luz.posicion - q).hat()).hat() ),100);// cout<<"[ "<<color.r<<", "<<color.g<<", "<<color.b<<" ]"<<endl;
            tmin = t;
        }
    }
    return color;
}



int main() {
    //Luz--------------------------------------------
    LuzPuntual luz(1,1,1,0,0,100);  
    // ESCENA----------------------------------------
    // Pacman
    Punto3D centro1(-50.0, 0.0, -200.0);
    double radioCuerpo = 130;
    Esfera cuerpo(centro1, radioCuerpo);   
    cuerpo.establecerColor(1.0, 1.0, 0.0);

    Punto3D centro2(-40.0, 85.0, 0.0);
    double radioOjo = 20;
    Esfera ojo(centro2, radioOjo);   
    ojo.establecerColor(0.0, 0.0, 0.0);

    Punto3D vertice1(-40.0, 0.0,-50.0);
    Punto3D vertice2(80.0,80.0,-50.0);
    Punto3D vertice3(80.0,-50.0,-50.0);
    Triangulo boca(vertice1,vertice2,vertice3);
    boca.establecerColor(0.0, 0.0, 0.0);

    // Puntos
    Punto3D centro3(50.0, 0.0, -10.0);
    Punto3D centro5(250.0, 0.0, -10.0);
    Punto3D centro6(450.0, 0.0, -10.0);
    Punto3D centro7(650.0, 0.0, -10.0);
    Punto3D centro9(850.0, 200.0, -10.0);
    Punto3D centro10(850.0, 400.0, -10.0);
    Punto3D centro11(850.0, -200.0, -10.0);
    Punto3D centro12(850.0, -400.0, -10.0);
    double radioPunto = 30;
    Esfera punto1(centro3, radioPunto);
    punto1.establecerColor(0.55, 0.55, 0.55);
    Esfera punto2(centro5, radioPunto);
    punto2.establecerColor(0.55, 0.55, 0.55);
    Esfera punto3(centro6, radioPunto);
    punto3.establecerColor(0.55, 0.55, 0.55);
    Esfera punto4(centro7, radioPunto);
    punto4.establecerColor(0.55, 0.55, 0.55);
    Esfera punto6(centro9, radioPunto);
    punto6.establecerColor(0.55, 0.55, 0.55);
    Esfera punto7(centro10, radioPunto);
    punto7.establecerColor(0.55, 0.55, 0.55);
    Esfera punto8(centro11, radioPunto);
    punto8.establecerColor(0.55, 0.55, 0.55);
    Esfera punto9(centro12, radioPunto);  
    punto9.establecerColor(0.55, 0.55, 0.55);

    Punto3D centro13(850.0, -600.0, -10.0);
    Punto3D centro14(850.0, -800.0, -10.0);
    Punto3D centro15(1050.0, -800.0, -10.0);
    Punto3D centro16(650.0, -800.0, -10.0);
    Punto3D centro17(450.0, -800.0, -10.0);
    Punto3D centro18(250.0, -800.0, -10.0);
    Punto3D centro19(50.0, -800.0, -70.0);
    Punto3D centro20(-150.0, -800.0, -10.0);
    Punto3D centro21(-350.0, -800.0, -10.0);
    Punto3D centro22(-550.0, -800.0, -10.0);
    Esfera punto10(centro13, radioPunto);
    punto10.establecerColor(0.55, 0.55, 0.55);
    Esfera punto11(centro14, radioPunto);
    punto11.establecerColor(0.55, 0.55, 0.55);
    Esfera punto12(centro15, radioPunto);
    punto12.establecerColor(0.55, 0.55, 0.55);
    Esfera punto13(centro16, radioPunto);
    punto13.establecerColor(0.55, 0.55, 0.55);
    Esfera punto14(centro16, radioPunto);
    punto14.establecerColor(0.55, 0.55, 0.55);
    Esfera punto15(centro17, radioPunto);
    punto15.establecerColor(0.55, 0.55, 0.55);
    Esfera punto16(centro18, radioPunto);
    punto16.establecerColor(0.55, 0.55, 0.55);
    Esfera punto17(centro19, 50);
    punto17.establecerColor(0.55, 0.55, 0.55);
    Esfera punto18(centro20, radioPunto);
    punto18.establecerColor(0.55, 0.55, 0.55);
    Esfera punto19(centro21, radioPunto);
    punto19.establecerColor(0.55, 0.55, 0.55);
    Esfera punto20(centro22, radioPunto);
    punto20.establecerColor(0.55, 0.55, 0.55);
    Punto3D centro23(850.0, 600.0, -10.0);
    Punto3D centro25(1050.0, 800.0, -10.0);
    Punto3D centro26(650.0, 800.0, -10.0);
    Punto3D centro27(450.0, 800.0, -10.0);
    Punto3D centro28(250.0, 800.0, -10.0);
    Punto3D centro29(50.0, 800.0, -10.0);
    Punto3D centro30(-150.0, 800.0, -10.0);
    Punto3D centro31(-350.0, 800.0, -10.0);
    Punto3D centro32(-550.0, 800.0, -10.0);
    punto10.establecerColor(0.55, 0.55, 0.55);
    Esfera punto21(centro23, radioPunto);
    punto21.establecerColor(0.55, 0.55, 0.55);
    Esfera punto23(centro25, radioPunto);
    punto23.establecerColor(0.55, 0.55, 0.55);
    Esfera punto24(centro26, radioPunto);
    punto24.establecerColor(0.55, 0.55, 0.55);
    Esfera punto25(centro27, radioPunto);
    punto25.establecerColor(0.55, 0.55, 0.55);
    Esfera punto26(centro28, radioPunto);
    punto26.establecerColor(0.55, 0.55, 0.55);
    Esfera punto27(centro29, radioPunto);
    punto27.establecerColor(0.55, 0.55, 0.55);
    Esfera punto28(centro30, radioPunto);
    punto28.establecerColor(0.55, 0.55, 0.55);
    Esfera punto29(centro31, radioPunto);
    punto29.establecerColor(0.55, 0.55, 0.55);
    Esfera punto30(centro32, radioPunto);
    punto30.establecerColor(0.55, 0.55, 0.55);

    Punto3D centro33(-750.0, 800.0, -10.0);
    Punto3D centro34(-950.0, 800.0, -10.0);
    Punto3D centro35(-1150.0, 800.0, -10.0);
    Punto3D centro36(-850.0, 600.0, -10.0);
    Punto3D centro37(-850.0, 400.0, -10.0);
    Punto3D centro38(-850.0, 200.0, -10.0);


    Esfera punto31(centro33, radioPunto);
    punto31.establecerColor(0.55, 0.55, 0.55);
    Esfera punto32(centro34, radioPunto);
    punto32.establecerColor(0.55, 0.55, 0.55);
    Esfera punto33(centro35, radioPunto);
    punto33.establecerColor(0.55, 0.55, 0.55);
    Esfera punto34(centro36, radioPunto);
    punto34.establecerColor(0.55, 0.55, 0.55);

    Esfera punto35(centro37, radioPunto);
    punto35.establecerColor(0.55, 0.55, 0.55);
    Esfera punto36(centro38, radioPunto);
    punto36.establecerColor(0.55, 0.55, 0.55);

    Punto3D VerticeInfeIzqPared1(1000.0, -660.0, -10.0);
    Punto3D VerticeSupIzqPared1(1000.0, 600.0, -10.0);
    Punto3D VerticeInfeDerPared1(1250.0, -660.0, -10.0);
    Punto3D VerticeSupDerPared1(1250.0, 600.0, -10.0);

    Punto3D VerticeInfeIzqPared12(1020.0, -640.0, -9.0);
    Punto3D VerticeSupIzqPared12(1020.0, 580.0, -9.0);
    Punto3D VerticeInfeDerPared12(1230.0, -640.0, -9.0);
    Punto3D VerticeSupDerPared12(1230.0, 580.0, -9.0);
    
    Triangulo paredIzq1(VerticeInfeIzqPared1,VerticeSupIzqPared1,VerticeInfeDerPared1);
    Triangulo paredIzq2(VerticeInfeDerPared1,VerticeSupIzqPared1,VerticeSupDerPared1);
    paredIzq1.establecerColor(0.1, 0.55, 0.55);
    paredIzq2.establecerColor(0.1, 0.55, 0.55);
   
    Triangulo paredIzq12(VerticeInfeIzqPared12,VerticeSupIzqPared12,VerticeInfeDerPared12);
    Triangulo paredIzq22(VerticeInfeDerPared12,VerticeSupIzqPared12,VerticeSupDerPared12);
    paredIzq12.establecerColor(0.0,0.0,0.0);
    paredIzq22.establecerColor(0.0,0.0,0.0);

    Punto3D VerticeInfeDerPared2(700.0, 150.0, -400.0);
    Punto3D VerticeSupDerPared2(700.0, 660.0, -400.0);
    Punto3D VerticeInfeizqPared2(-700.0, 150.0, -400.0);
    Punto3D VerticeSupizqPared2(-700.0, 660.0, -400.0);
    Triangulo paredsup1(VerticeInfeDerPared2,VerticeSupDerPared2,VerticeSupizqPared2);
    Triangulo paredsup2(VerticeSupizqPared2,VerticeInfeizqPared2,VerticeInfeDerPared2);
    paredsup1.establecerColor(0.1, 0.55, 0.55);
    paredsup2.establecerColor(0.1, 0.55, 0.55);

    Punto3D VerticeInfeDerrelleno2(680.0, 170.0, -395.0);
    Punto3D VerticeSupDerrelleno2(680.0, 640.0, -395.0);
    Punto3D VerticeInfeizqrelleno2(-680.0, 170.0, -395.0);
    Punto3D VerticeSupizqrelleno2(-680.0, 640.0, -395.0);
    Triangulo paredsuprelleno(VerticeInfeDerrelleno2,VerticeSupDerrelleno2,VerticeSupizqrelleno2);
    Triangulo paredsuprelleno2(VerticeSupizqrelleno2,VerticeInfeizqrelleno2,VerticeInfeDerrelleno2);
    paredsuprelleno.establecerColor(0.0,0.0,0.0);
    paredsuprelleno2.establecerColor(0.0,0.0,0.0);

    Punto3D VerticeInfeDerPared3(700.0, -150.0, -10.0);
    Punto3D VerticeSupDerPared3(700.0, -660.0, -10.0);
    Punto3D VerticeInfeizqPared3(200.0, -150.0, -10.0);
    Punto3D VerticeSupizqPared3(200.0, -660.0, -10.0);
    Triangulo paredINF1(VerticeInfeDerPared3,VerticeSupDerPared3,VerticeSupizqPared3);
    Triangulo paredINF2(VerticeSupizqPared3,VerticeInfeizqPared3,VerticeInfeDerPared3);
    paredINF1.establecerColor(0.1, 0.55, 0.55);
    paredINF2.establecerColor(0.1, 0.55, 0.55);
  
    Punto3D VerticeInfeDerrelleno3(680.0, -170.0, -9.0);
    Punto3D VerticeSupDerrelleno3(680.0, -640.0, -9.0);
    Punto3D VerticeInfeizqrelleno3(220.0, -170.0, -9.0);
    Punto3D VerticeSupizqrelleno3(220.0, -640.0, -9.0);
    Triangulo paredinfrelleno(VerticeInfeDerrelleno3,VerticeSupDerrelleno3,VerticeSupizqrelleno3);
    Triangulo paredinfrelleno2(VerticeSupizqrelleno3,VerticeInfeizqrelleno3,VerticeInfeDerrelleno3);
    paredinfrelleno.establecerColor(0.0,0.0,0.0);
    paredinfrelleno2.establecerColor(0.0,0.0,0.0);

    Punto3D VerticeInfeDerPared4(-700.0, -150.0, -10.0);
    Punto3D VerticeSupDerPared4(-700.0, -660.0, -10.0);
    Punto3D VerticeInfeizqPared4(-200.0, -150.0, -10.0);
    Punto3D VerticeSupizqPared4(-200.0, -660.0, -10.0);
    Triangulo paredINF21(VerticeInfeDerPared4,VerticeSupDerPared4,VerticeSupizqPared4);
    Triangulo paredINF22(VerticeSupizqPared4,VerticeInfeizqPared4,VerticeInfeDerPared4);
    paredINF21.establecerColor(0.1, 0.55, 0.55);
    paredINF22.establecerColor(0.1, 0.55, 0.55);
  
    Punto3D VerticeInfeDerrelleno4(-680.0, -170.0, -9.0);
    Punto3D VerticeSupDerrelleno4(-680.0, -640.0, -9.0);
    Punto3D VerticeInfeizqrelleno4(-220.0, -170.0, -9.0);
    Punto3D VerticeSupizqrelleno4(-220.0, -640.0, -9.0);
    Triangulo paredinfrelleno21(VerticeInfeDerrelleno4,VerticeSupDerrelleno4,VerticeSupizqrelleno4);
    Triangulo paredinfrelleno22(VerticeSupizqrelleno4,VerticeInfeizqrelleno4,VerticeInfeDerrelleno4);
    paredinfrelleno21.establecerColor(0.0,0.0,0.0);
    paredinfrelleno22.establecerColor(0.0,0.0,0.0);

    Punto3D VerticeInfeIzqPared6(-1000.0, -660.0, -10.0);
    Punto3D VerticeSupIzqPared6(-1000.0, 660.0, -10.0);
    Punto3D VerticeInfeDerPared6(-1250.0, -660.0, -10.0);
    Punto3D VerticeSupDerPared6(-1250.0, 660.0, -10.0);

    Punto3D VerticeInfeIzqPared61(-1020.0, -640.0, -9.0);
    Punto3D VerticeSupIzqPared61(-1020.0, 640.0, -9.0);
    Punto3D VerticeInfeDerPared61(-1230.0, -640.0, -9.0);
    Punto3D VerticeSupDerPared61(-1230.0, 640.0, -9.0);
    
    Triangulo paredIzq6(VerticeInfeIzqPared6,VerticeSupIzqPared6,VerticeInfeDerPared6);
    Triangulo paredIzq61(VerticeInfeDerPared6,VerticeSupIzqPared6,VerticeSupDerPared6);
    paredIzq6.establecerColor(0.1, 0.55, 0.55);
    paredIzq61.establecerColor(0.1, 0.55, 0.55);
     Triangulo paredIzq7(VerticeInfeIzqPared61,VerticeSupIzqPared61,VerticeInfeDerPared61);
    Triangulo paredIzq71(VerticeInfeDerPared61,VerticeSupIzqPared61,VerticeSupDerPared61);
    paredIzq7.establecerColor(0.0, 0.0, 0.0);
    paredIzq71.establecerColor(0.0, 0.0, 0.0);

    // Fantasma
    Punto3D centro4 (-500.0, 10.0, -180.0);
    double radioFantasma = 120;
    Esfera cabezaFantasma(centro4,radioFantasma);
    Punto3D vertice4(-620.0, 0.0, -80.0);
    Punto3D vertice5(-380.0, 0.0, -80.0);
    Punto3D vertice6(-620.0,-120.0,-80.0);
    Triangulo fantasma1(vertice4,vertice5,vertice6);
    Punto3D vertice7(-380.0, -120.0, -80.0);
    Triangulo fantasma2(vertice5,vertice7,vertice6);
    cabezaFantasma.establecerColor(0.55, 0.55, 0.55);
    fantasma1.establecerColor(0.55, 0.55, 0.55);
    fantasma2.establecerColor(0.55, 0.55, 0.55);
    
    Punto3D puntoOjoFantasma(-550.0,30.0,-90.0);
    double radioOjoFantasma = 30;
    Esfera ojoFantasma(puntoOjoFantasma,radioOjoFantasma);
    ojoFantasma.establecerColor(1.0,1.0,1.0);
    
    Punto3D puntoOjoFantasma2(-450.0,30.0,-90.0);
    double radioOjoFantasma2 = 30;
    Esfera ojoFantasma2(puntoOjoFantasma2,radioOjoFantasma2);
    ojoFantasma2.establecerColor(1.0,1.0,1.0);
    
    Punto3D puntoIrisFantasma(-545.0,30.0,-65.0);
    double radioIrisFantasma = 12;
    Esfera irisFantasma(puntoIrisFantasma,radioIrisFantasma);
    irisFantasma.establecerColor(0.0,0.0,0.0);
    
    Punto3D puntoIrisFantasma2(-435.0,30.0,-60.0);
    double radioIrisFantasma2 = 12;
    Esfera irisFantasma2(puntoIrisFantasma2,radioIrisFantasma2);
    irisFantasma2.establecerColor(0.0,0.0,0.0);
    
    Punto3D vertice8(-620.0, -120.0, -70.0);
    Punto3D vertice9(-580.0, -80.0, -70.0);
    Punto3D vertice10(-540.0, -120.0, -70.0);
    Punto3D vertice11(-500.0, -80.0, -70.0);
    Punto3D vertice12(-460.0, -120.0, -70.0);
    Punto3D vertice13(-420.0, -80.0, -70.0);
    Punto3D vertice14(-380.0, -120.0, -70.0);

    Triangulo fantasma3(vertice8,vertice9,vertice10);
    Triangulo fantasma4(vertice10,vertice11,vertice12);
    Triangulo fantasma5(vertice12,vertice13,vertice14);

    fantasma3.establecerColor(0.0, 0.0, 0.0);
    fantasma4.establecerColor(0.0, 0.0, 0.0);
    fantasma5.establecerColor(0.0, 0.0, 0.0);

    // Fantasma Blinky
    Punto3D centroBlinky (0.0, -350.0, -180.0);
    Esfera cabezaBlinky(centroBlinky,radioFantasma);
    Punto3D verticeBlinky1(-120.0, -360.0, -80.0);
    Punto3D verticeBlinky2(120.0, -360.0, -80.0);
    Punto3D verticeBlinky3(-120.0,-480.0,-80.0);
    Triangulo cuerpoBlinky1(verticeBlinky1,verticeBlinky2,verticeBlinky3);
    Punto3D verticeBlinky4(120.0, -480.0, -80.0);
    Triangulo cuerpoBlinky2(verticeBlinky2,verticeBlinky4,verticeBlinky3);
    cabezaBlinky.establecerColor(1.0, 0.0, 0.0);
    cuerpoBlinky1.establecerColor(1.0, 0.0, 0.0);
    cuerpoBlinky2.establecerColor(1.0, 0.0, 0.0);

    Punto3D puntoOjoBlinky1(-50.0,-330.0,-90.0);
    Esfera ojoBlinky1(puntoOjoBlinky1,radioOjoFantasma);
    ojoBlinky1.establecerColor(1.0,1.0,1.0);
     
    Punto3D puntoOjoBlinky2(50.0,-330.0,-90.0);
    Esfera ojoBlinky2(puntoOjoBlinky2,radioOjoFantasma);
    ojoBlinky2.establecerColor(1.0,1.0,1.0);
    
    Punto3D puntoIrisBlinky(-55.5,-320.0,-65.0);
    Esfera irisBlinky1(puntoIrisBlinky,radioIrisFantasma);
    irisBlinky1.establecerColor(0.0,0.0,0.0);
     
    Punto3D puntoIrisBlinky2(55.5,-320.0,-65.0);
    Esfera irisBlinky2(puntoIrisBlinky2,radioIrisFantasma);
    irisBlinky2.establecerColor(0.0,0.0,0.0);
    
    Punto3D verticeBlinky5(-120.0, -480.0, -70.0);
    Punto3D verticeBlinky6(-80.0, -440.0, -70.0);
    Punto3D verticeBlinky7(-40.0, -480.0, -70.0);
    Punto3D verticeBlinky8(0.0, -440.0, -70.0);
    Punto3D verticeBlinky9(40.0, -480.0, -70.0);
    Punto3D verticeBlinky10(80.0, -440.0, -70.0);
    Punto3D verticeBlinky11(120.0, -480.0, -70.0);

    Triangulo trianguloBlinky1(verticeBlinky5,verticeBlinky6,verticeBlinky7);
    Triangulo trianguloBlinky2(verticeBlinky7,verticeBlinky8,verticeBlinky9);
    Triangulo trianguloBlinky3(verticeBlinky9,verticeBlinky10,verticeBlinky11);

    trianguloBlinky1.establecerColor(0.0, 0.0, 0.0);
    trianguloBlinky2.establecerColor(0.0, 0.0, 0.0);
    trianguloBlinky3.establecerColor(0.0, 0.0, 0.0);


 // Fantasma Pinky
    Punto3D centroPinky (400.0, 450.0, -180.0);
    Esfera cabezaFantasmaPinky(centroPinky,radioFantasma);
    Punto3D vertice4Pinky(280.0, 440.0, -80.0);
    Punto3D vertice5Pinky(520.0, 440.0, -80.0);
    Punto3D vertice6Pinky(280.0, 320.0,-80.0);
    Punto3D vertice7Pinky(520.0, 320.0, -80.0);
    Triangulo fantasma1Pinky(vertice4Pinky,vertice5Pinky,vertice6Pinky);
    Triangulo fantasma2Pinky(vertice5Pinky,vertice7Pinky,vertice6Pinky);
    cabezaFantasmaPinky.establecerColor(1.00, 0.75, 0.79);
    fantasma1Pinky.establecerColor(1.00, 0.75, 0.79);
    fantasma2Pinky.establecerColor(1.00, 0.75, 0.79);
    
    double radioOjoFantasmaPinky = 30;

    Punto3D puntoOjoFantasmaPinky(350.0,470.0,-90.0);
    Esfera ojoFantasmaPinky(puntoOjoFantasmaPinky,radioOjoFantasmaPinky);
    ojoFantasmaPinky.establecerColor(1.0,1.0,1.0);
    
    Punto3D puntoOjoFantasma2Pinky(450.0,470.0,-90.0);
    Esfera ojoFantasma2Pinky(puntoOjoFantasma2Pinky,radioOjoFantasmaPinky);
    ojoFantasma2Pinky.establecerColor(1.0,1.0,1.0);

    Punto3D puntoIrisFantasmaPinky(333.0,470.0,-65.0);
    Esfera irisFantasmaPinky(puntoIrisFantasmaPinky,radioIrisFantasma);
    irisFantasmaPinky.establecerColor(0.0,0.0,0.0);
    
    Punto3D puntoIrisFantasma2Pinky(445.0,470.0,-60.0);
    Esfera irisFantasma2Pinky(puntoIrisFantasma2Pinky,radioIrisFantasma);
    irisFantasma2Pinky.establecerColor(0.0,0.0,0.0);

    Punto3D vertice8Pinky(280.0, 320.0, -70.0);
    Punto3D vertice9Pinky(320.0, 360.0, -70.0);
    Punto3D vertice10Pinky(360.0, 320.0, -70.0);
    Punto3D vertice11Pinky(400.0, 360.0, -70.0);
    Punto3D vertice12Pinky(440.0, 320.0, -70.0);
    Punto3D vertice13Pinky(480.0, 360.0, -70.0);
    Punto3D vertice14Pinky(520.0, 320.0, -70.0);

    Triangulo fantasma3Pinky(vertice8Pinky,vertice9Pinky,vertice10Pinky);
    Triangulo fantasma4Pinky(vertice10Pinky,vertice11Pinky,vertice12Pinky);
    Triangulo fantasma5Pinky(vertice12Pinky,vertice13Pinky,vertice14Pinky);

    fantasma3Pinky .establecerColor(0.0, 0.0, 0.0);
    fantasma4Pinky.establecerColor(0.0, 0.0, 0.0);
    fantasma5Pinky.establecerColor(0.0, 0.0, 0.0);

    // FantasmaNaranja
    Punto3D centroFantasmaNaranja (-400.0, 450.0, -180.0);
    Esfera cabezaFantasmaNaranja(centroFantasmaNaranja,radioFantasma);
    cabezaFantasmaNaranja.establecerColor(0.9411, 0.5098, 0);
    Punto3D verticeFantasmaNaranja1(-520.0, 440.0, -80.0);
    Punto3D verticeFantasmaNaranja2(-280.0, 440.0, -80.0);
    Punto3D verticeFantasmaNaranja3(-520.0,320.0,-80.0);
    Punto3D verticeFantasmaNaranja4(-280.0, 320.0, -80.0);
    Triangulo cuerpoFantasmaNaranja1(verticeFantasmaNaranja1,verticeFantasmaNaranja2,verticeFantasmaNaranja3);
    Triangulo cuerpoFantasmaNaranja2(verticeFantasmaNaranja2,verticeFantasmaNaranja4,verticeFantasmaNaranja3);
    cuerpoFantasmaNaranja1.establecerColor(0.9411, 0.5098, 0);
    cuerpoFantasmaNaranja2.establecerColor(0.9411, 0.5098, 0);
    
    Punto3D puntoOjoFantasmaNaranja(-450.0,470.0,-90.0);
    Esfera ojoFantasmaNaranja(puntoOjoFantasmaNaranja,radioOjoFantasma);
    ojoFantasmaNaranja.establecerColor(1.0,1.0,1.0);
    
    Punto3D puntoOjoFantasmaNaranja2(-350.0,470.0,-90.0);
    Esfera ojoFantasmaNaranja2(puntoOjoFantasmaNaranja2,radioOjoFantasma2);
    ojoFantasmaNaranja2.establecerColor(1.0,1.0,1.0);
    
    Punto3D puntoIrisFantasmaNaranja(-445.0,470.0,-65.0);
    Esfera irisFantasmaNaranja(puntoIrisFantasmaNaranja,radioIrisFantasma);
    irisFantasmaNaranja.establecerColor(0.0,0.0,0.0);
     
    Punto3D puntoIrisFantasmaNaranja2(-335.0,470.0,-60.0);
    Esfera irisFantasmaNaranja2(puntoIrisFantasmaNaranja2,radioIrisFantasma);
    irisFantasmaNaranja2.establecerColor(0.0,0.0,0.0);
    
    Punto3D verticeInferiorFantasmaNaranja1(-520.0, 320.0, -70.0);
    Punto3D verticeInferiorFantasmaNaranja2(-480.0, 360.0, -70.0);
    Punto3D verticeInferiorFantasmaNaranja3(-440.0, 320.0, -70.0);
    Punto3D verticeInferiorFantasmaNaranja4(-400.0, 360.0, -70.0);
    Punto3D verticeInferiorFantasmaNaranja5(-360.0, 320.0, -70.0);
    Punto3D verticeInferiorFantasmaNaranja6(-320.0, 360.0, -70.0);
    Punto3D verticeInferiorFantasmaNaranja7(-280.0, 320.0, -70.0);

    Triangulo piesFantasmaNaranja1(verticeInferiorFantasmaNaranja1,verticeInferiorFantasmaNaranja2,verticeInferiorFantasmaNaranja3);
    Triangulo piesFantasmaNaranja2(verticeInferiorFantasmaNaranja3,verticeInferiorFantasmaNaranja4,verticeInferiorFantasmaNaranja5);
    Triangulo piesFantasmaNaranja3(verticeInferiorFantasmaNaranja5,verticeInferiorFantasmaNaranja6,verticeInferiorFantasmaNaranja7);

    piesFantasmaNaranja1.establecerColor(0.0, 0.0, 0.0);
    piesFantasmaNaranja2.establecerColor(0.0, 0.0, 0.0);
    piesFantasmaNaranja3.establecerColor(0.0, 0.0, 0.0);

    //Puerta Fantasmas
    Punto3D verticeSuperiorIzquierdo(-250, 665, -100.0);
    Punto3D verticeSuperiorDerecho(250, 665, -100.0);
    Punto3D verticeInferiorIzquierdo(-250, 635, -100.0);
    Punto3D verticeInferiorDerecho(250, 635, -100.0);

    Triangulo triangulo1(verticeSuperiorIzquierdo,verticeSuperiorDerecho,verticeInferiorIzquierdo);
    Triangulo triangulo2(verticeInferiorIzquierdo,verticeSuperiorDerecho,verticeInferiorDerecho);

    triangulo1.establecerColor(1.0,1.0,1.0);
    triangulo2.establecerColor(1.0,1.0,1.0);

    vector<ObjetoGeometrico*> escena;
    //Cereza

    Punto3D centroCereza1(800.0, 0.0, -70.0);
    double radioCereza = 45;
    Esfera cereza1(centroCereza1,radioCereza);
    cereza1.establecerColor(1.0,0.0,0.0);
    
    Punto3D centroCereza2(870.0, -15.0, -10.0);
    Esfera cereza2(centroCereza2,radioCereza);
    cereza2.establecerColor(1.0,0.0,0.0);
   
    Punto3D puntoTallo1(810.0, 40.0,-70.0);
    Punto3D puntoTallo2(790.0, 40.0,-70.0);
    Punto3D puntoTallo3(810.0, 70.0,-70.0);
    Triangulo trianguloTallo1(puntoTallo1,puntoTallo2,puntoTallo3);
    trianguloTallo1.establecerColor(0.580,0.416,0.251);
    
    Punto3D puntoTallo4(790.0, 80.0,-70.0);
    
    Triangulo trianguloTallo2(puntoTallo2,puntoTallo3,puntoTallo4);
    trianguloTallo2.establecerColor(0.580,0.416,0.251);

    Punto3D puntoTallo5(825.0, 120.0,-70.0);
    
    Triangulo trianguloTallo3(puntoTallo5,puntoTallo3,puntoTallo4);
    trianguloTallo3.establecerColor(0.580,0.416,0.251);

    Punto3D puntoTallo6(850.0, 135.0,-70.0);
    Punto3D puntoTallo7(805.0, 80.0,-70.0);

    Triangulo trianguloTallo4(puntoTallo5,puntoTallo6,puntoTallo7);
    trianguloTallo4.establecerColor(0.580,0.416,0.251);

    
    Punto3D puntoTallo8(815.0, 100.0,-70.0);
    Punto3D puntoTallo9(840.0, 40.0,-70.0);

    Triangulo trianguloTallo5(puntoTallo5,puntoTallo8,puntoTallo9);
    trianguloTallo5.establecerColor(0.368,0.266,0.161);
    
    Punto3D puntoTallo10(850.0, -15.0,-70.0);
    Punto3D puntoTallo11(890.0, -15.0,-70.0);


    Triangulo trianguloTallo8(puntoTallo8,puntoTallo10,puntoTallo11);
    trianguloTallo8.establecerColor(0.580,0.416,0.251);

    Punto3D centroEsfera11(-850.0,-300.0,-70);
    double radioEsfera11 = 90;
    Esfera esfera11(centroEsfera11,radioEsfera11);
    esfera11.establecerColor(0.5, 0.2 , 0.2);

    // Manzana
    Punto3D puntoManzana1(-850.0,0.0,-70);
    double centroManzana1 =50;
    Esfera circuloManzana1(puntoManzana1,centroManzana1);
    circuloManzana1.establecerColor(0.9,0,0);
    
    Punto3D puntoTrianguloManzana1(-850.0,-20.0,-10);
    Punto3D puntoTrianguloManzana2(-830.0,-60.0,-10);
    Punto3D puntoTrianguloManzana3(-870.0,-60.0,-10);
    Triangulo trianguloManzana(puntoTrianguloManzana1,puntoTrianguloManzana2,puntoTrianguloManzana3);
    trianguloManzana.establecerColor(0,0,0);

    Punto3D baseManzana1(-900.0,-40.0,-10);
    Punto3D baseManzana2(-800.0,-40.0,-10);
    Punto3D baseManzana3(-850.0,-70.0,-10);
    Triangulo baseManzana(baseManzana1,baseManzana2,baseManzana3);
    baseManzana.establecerColor(0,0,0);

    Punto3D puntoTallo12(-855.0, 30.0,-10.0);
    Punto3D puntoTallo22(-845.0, 30.0,-10.0);
    Punto3D puntoTallo32(-865.0, 70.0,-10.0);
    Triangulo trianguloTallo12(puntoTallo12,puntoTallo22,puntoTallo32);
    trianguloTallo12.establecerColor(0.680,0.4516,0.251);

    
    Punto3D puntoTallo42(-846.0, 80.0,-10.0);

    Triangulo trianguloTallo22(puntoTallo22,puntoTallo32,puntoTallo42);
    trianguloTallo22.establecerColor(0.680,0.4516,0.251);

    Punto3D hoja1(-850.0, 70.0,-70.0);
    Punto3D hoja2(-825.0, 70.0,-70.0); 
    Punto3D hoja3(-825.0, 90.0,-70.0);  
    Triangulo hoja(hoja1,hoja2,hoja3);
    hoja.establecerColor(0,1.0,0);

//===============================Durazno======================================

    double radioDurazano= 50;
    Punto3D centroDurazno(850.0, 785.0, -60.0);
    Esfera durazno(centroDurazno, radioDurazano);
    durazno.establecerColor(0.55, 0.55, 0.55);

    

    Punto3D puntoTalloDurazno12(865.0, 820.0,-10.0);
    Punto3D puntoTalloDurazno22(855.0, 820.0,-10.0);
    Punto3D puntoTalloDurazno32(835.0, 860.0,-10.0);
    Triangulo trianguloDurazno1(puntoTalloDurazno12,puntoTalloDurazno22,puntoTalloDurazno32);
    trianguloDurazno1.establecerColor(0.680,0.4516,0.251);
    Punto3D puntoTalloDurazno42(856.0, 870.0,-10.0);
    Triangulo trianguloDurazno2(puntoTalloDurazno22,puntoTalloDurazno32,puntoTalloDurazno42);
    trianguloDurazno2.establecerColor(0.680,0.4516,0.251);

 

    //Naranja
    Punto3D talloNaranja(45.0, -770.0,-10.0);
    Punto3D talloNaranja1(55.0, -770.0,-10.0);
    Punto3D talloNaranja2(35.0, -730.0,-10.0);

    Triangulo talloNaranjaa(talloNaranja,talloNaranja1,talloNaranja2);
    talloNaranjaa.establecerColor(0.680,0.4516,0.251);

    Punto3D talloNaranja3(56.0, -720.0,-10.0);

    Triangulo talloNaranjaaa(talloNaranja1,talloNaranja2,talloNaranja3);
    talloNaranjaaa.establecerColor(0.680,0.4516,0.251);

    Punto3D hojaNaranja(50.0, -730.0,-10.0);
    Punto3D hojaNaranja2(75.0, -730.0,-10.0); 
    Punto3D hojaNaranja3(75.0, -710.0,-10.0);  
    Triangulo hojaNaranjaa(hojaNaranja,hojaNaranja2,hojaNaranja3);
    hojaNaranjaa.establecerColor(0,1.0,0);

     //uva
    double radioUva1=25;
    Punto3D centroUva1(0.0, 800.0, -10.0);
    Punto3D centroUva2(22.0, 815.0, -10.0);
    Punto3D centroUva3(25.0, 785.0, -30.0);
    Punto3D centroUva4(-5.0, 760.0, -25.0);
    Punto3D centroUva5(45.0, 770.0, -25.0);
    Punto3D centroUva6(50.0, 785.0, -10.0);
    Punto3D centroUva7(25.0, 745.0, -35.0);
    Punto3D centroUva8(30.0, 755.0, -15.0);
    Punto3D centroUva9(60.0, 745.0, -15.0);

    
    double radioUva2=20;
    double radioUva3=15;
    
    Esfera uva1(centroUva1, radioUva1);
    Esfera uva2(centroUva2, radioUva1);
    Esfera uva3(centroUva3, radioUva2);
    Esfera uva4(centroUva4, radioUva2);
    Esfera uva5(centroUva5, radioUva2);
    Esfera uva6(centroUva6, radioUva2);
    Esfera uva7(centroUva7, radioUva2);
    Esfera uva8(centroUva8, radioUva3);
    Esfera uva9(centroUva9, radioUva3);


    uva1.establecerColor(0.0, 1.0, 0.0);
    uva2.establecerColor(0.0, 1.0, 0.0);
    uva3.establecerColor(0.0, 1.0, 0.0);
    uva4.establecerColor(0.0, 1.0, 0.0);
    uva5.establecerColor(0.0, 1.0, 0.0);
    uva6.establecerColor(0.0, 1.0, 0.0);
    uva7.establecerColor(0.0, 1.0, 0.0);
    uva8.establecerColor(0.0, 1.0, 0.0);
    uva9.establecerColor(0.0, 1.0, 1.0);

    Punto3D puntoUva1(-55.0, 815.0,-10);
    Punto3D puntoUva2(-55.0, 815.0,-10);
    Punto3D puntoUva3(-65.0, 835.0,-10);
    Punto3D puntoUva4(-56.0, 865.0,-10);
    Triangulo talloUva1(puntoUva1,puntoUva2,puntoUva3);
    Triangulo talloUva2(puntoUva2,puntoUva3,puntoUva4);
    
    talloUva1.establecerColor(0.580,0.416,0.251);
    talloUva2.establecerColor(0.580,0.416,0.251);
   


//==============================Texturas========================================
    Image img1;
    Image img2;
    Image img6;
    Image img4;
    Image img5;


    img1.read_ppm_file("manzanaVerde.ppm");
    img2.read_ppm_file("cereza.ppm");
    img5.read_ppm_file("duraznoHoja.ppm");
    img4.read_ppm_file("naranja.ppm");
    img6.read_ppm_file("uva.ppm");
    
    circuloManzana1.establecerTextura(img1);
    cereza1.establecerTextura(img2);
    cereza2.establecerTextura(img2);
    durazno.establecerTextura(img5);
    punto17.establecerTextura(img4);
     uva1.establecerTextura(img6);
     uva2.establecerTextura(img6);
     uva3.establecerTextura(img6);
     uva4.establecerTextura(img6);
     uva5.establecerTextura(img6);
     uva6.establecerTextura(img6);
     uva7.establecerTextura(img6);
     uva8.establecerTextura(img6);
     uva9.establecerTextura(img6);
    
//=============================================

    // escena.push_back(&esfera11);
    escena.push_back(&trianguloTallo22);
    escena.push_back(&trianguloTallo12);    
    escena.push_back(&circuloManzana1);
    // escena.push_back(&trianguloManzana);
    // escena.push_back(&baseManzana);
    escena.push_back(&hoja);

    //Pared Fantasma
    escena.push_back(&triangulo1);
    escena.push_back(&triangulo2);

    //Pacman
    escena.push_back(&boca);
    escena.push_back(&cuerpo);
    escena.push_back(&ojo);
  
    // Puntos
    escena.push_back(&punto1);
    escena.push_back(&punto2);
    escena.push_back(&punto3);
    escena.push_back(&punto4);
    escena.push_back(&punto6);
    escena.push_back(&punto7);
    escena.push_back(&punto8);
    escena.push_back(&punto9);
    escena.push_back(&punto10);
    escena.push_back(&punto11);
    escena.push_back(&punto12);
    escena.push_back(&punto13);
    escena.push_back(&punto14);
    escena.push_back(&punto15);
    escena.push_back(&punto16);
    escena.push_back(&punto17);
    escena.push_back(&punto18);
    escena.push_back(&punto19);
    escena.push_back(&punto20);
    escena.push_back(&punto21);
    escena.push_back(&punto23);
    escena.push_back(&punto24);
    escena.push_back(&punto25);
    escena.push_back(&punto26);
    escena.push_back(&punto28);
    escena.push_back(&punto29);
    escena.push_back(&punto30);
    escena.push_back(&punto31);
    escena.push_back(&punto32);
    escena.push_back(&punto33);
    escena.push_back(&punto34);
    escena.push_back(&punto35);
    escena.push_back(&punto36);
    
    // Fantasma
    escena.push_back(&cabezaFantasma);
    escena.push_back(&fantasma1);
    escena.push_back(&fantasma2);
    escena.push_back(&fantasma3);
    escena.push_back(&fantasma4);
    escena.push_back(&fantasma5);
    escena.push_back(&ojoFantasma);
    escena.push_back(&ojoFantasma2);
    escena.push_back(&irisFantasma);
    escena.push_back(&irisFantasma2);

    //Fantasma Pinky
    escena.push_back(&cabezaFantasmaPinky);
    escena.push_back(&fantasma1Pinky);
    escena.push_back(&fantasma2Pinky);
    escena.push_back(&ojoFantasmaPinky);
    escena.push_back(&ojoFantasma2Pinky);
    escena.push_back(&irisFantasmaPinky);
    escena.push_back(&irisFantasma2Pinky);
    escena.push_back(&fantasma3Pinky);
    escena.push_back(&fantasma4Pinky);
    escena.push_back(&fantasma5Pinky);

    //Fantasma Blinky
    escena.push_back(&cabezaBlinky);
    escena.push_back(&cuerpoBlinky1);
    escena.push_back(&cuerpoBlinky2); 
    escena.push_back(&ojoBlinky1);
    escena.push_back(&ojoBlinky2);
    escena.push_back(&irisBlinky1);
    escena.push_back(&irisBlinky2);
    escena.push_back(&trianguloBlinky1);
    escena.push_back(&trianguloBlinky2);
    escena.push_back(&trianguloBlinky3);

    //FantasmaNaranja
    escena.push_back(&cabezaFantasmaNaranja);
    escena.push_back(&cuerpoFantasmaNaranja1);
    escena.push_back(&cuerpoFantasmaNaranja2);
    escena.push_back(&piesFantasmaNaranja1);
    escena.push_back(&piesFantasmaNaranja2);
    escena.push_back(&piesFantasmaNaranja3);
    escena.push_back(&ojoFantasmaNaranja);
    escena.push_back(&ojoFantasmaNaranja2);
    escena.push_back(&irisFantasmaNaranja);
    escena.push_back(&irisFantasmaNaranja2);

    //Pared
    escena.push_back(&paredIzq1);
    escena.push_back(&paredIzq2);
    escena.push_back(&paredIzq12);
    escena.push_back(&paredIzq22);
    escena.push_back(&paredsup1);
    escena.push_back(&paredsup2);
    escena.push_back(&paredsuprelleno);
    escena.push_back(&paredsuprelleno2);
    escena.push_back(&paredINF1);
    escena.push_back(&paredINF2);
    escena.push_back(&paredinfrelleno);
    escena.push_back(&paredinfrelleno2);
    escena.push_back(&paredINF21);
    escena.push_back(&paredINF22);
    escena.push_back(&paredinfrelleno22);
    escena.push_back(&paredinfrelleno21);
    escena.push_back(&paredIzq6);
    escena.push_back(&paredIzq61);
    escena.push_back(&paredIzq7);
    escena.push_back(&paredIzq71);

    //Cereza
    escena.push_back(&cereza2);
    escena.push_back(&cereza1);
    escena.push_back(&trianguloTallo1);
    escena.push_back(&trianguloTallo2);
    escena.push_back(&trianguloTallo3);
    escena.push_back(&trianguloTallo4);
    escena.push_back(&trianguloTallo5);
    escena.push_back(&trianguloTallo8);

    //Durazno
    escena.push_back(&durazno);
    escena.push_back(&trianguloDurazno1);
    escena.push_back(&trianguloDurazno2);


    

    //Naranja
    escena.push_back(&talloNaranjaa);
    escena.push_back(&talloNaranjaaa); 
    escena.push_back(&hojaNaranjaa);
    //Uvas
    escena.push_back(&uva1);
    escena.push_back(&uva2);
    escena.push_back(&uva3);
    escena.push_back(&uva4);
    escena.push_back(&uva5);
    escena.push_back(&uva6);
    escena.push_back(&uva7);
    escena.push_back(&uva8);
    escena.push_back(&uva9);
    escena.push_back(&talloUva1);
    escena.push_back(&talloUva2);
    
    ColorRGB color_pixel;

    // VIEWPLANE
    int hres = 2500;
    int vres = 1800;
    double s = 1.0;
    ViewPlane vp(hres, vres, s);
    
    // UTILITARIO PARA GUARDAR IMAGEN -------------------
    int dpi = 72;
    int width = vp.hres;
    int height = vp.vres;
    int n = width * height;
    ColorRGB* pixeles = new ColorRGB[n];

    // ALGORITMO
    for(int fil = 0; fil < vp.vres; fil++)
    {
        for ( int col = 0; col < vp.hres; col++) 
        {
             // Disparar un rayo
            Vector3D direccion(0.0, 0.0, -1.0);
            double x_o = vp.s * ( col - vp.hres/2 + 0.5 );
            double y_o = vp.s * ( fil - vp.vres/2 + 0.5 );
            double z_o = 100;
            Punto3D origen(x_o, y_o, z_o);
            Rayo rayo(origen, direccion);

            //color_pixel = obtenerColorPixel(rayo, escena,luz);

            pixeles[fil*width+col].r = obtenerColorPixel(rayo,escena,luz).r;
            pixeles[fil*width+col].g = obtenerColorPixel(rayo,escena,luz).g;
            pixeles[fil*width+col].b = obtenerColorPixel(rayo,escena,luz).b;
        }
    }    
    savebmp("img.bmp", width, height, dpi, pixeles);
    return 0;
}